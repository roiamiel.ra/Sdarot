package myapps.sdarot;

public class Series {
    public static final String NAME_TAG = "Name";
    public static final String SEASON_TAG = "Season";
    public static final String EPISODE_TAG = "Episode";

    private String mName;
    private int mSeason;
    private int mEpisode;

    public OnAddOne mOnAddOne;

    public Series(String Name, int Season, int Episode){
        setName(Name);
        setSeason(Season);
        setEpisode(Episode);

    }

    public Series(String Name, int Season, int Episode, OnAddOne OnAddOne){
        setName(Name);
        setSeason(Season);
        setEpisode(Episode);

        mOnAddOne = OnAddOne;

    }

    public String getName() {
        return mName;
    }

    public int getSeason() {
        return mSeason;
    }

    public int getEpisode() {
        return mEpisode;
    }

    public void setName(String Name) {
        this.mName = Name;
    }

    public void setSeason(int Season) {
        this.mSeason = Season;
    }

    public void setEpisode(int Episode) {
        this.mEpisode = Episode;
    }

    interface OnAddOne {
        public void onUserAddEpisode(int Position);
    }

}

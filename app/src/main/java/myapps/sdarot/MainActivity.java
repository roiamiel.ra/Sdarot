package myapps.sdarot;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RewardedVideoAdListener{

    private final static String SP_TAG = "Sdarot";
    private final static String SDAROT_JSONARRAY_TAG = "Sdarot";

    private AdView mAdView;

    private RecyclerView mRecyclerView;
    private FloatingActionButton mAddFAB;

    private Adapter mAdapter;
    private ArrayList<Series> mSessionsList;

    private SharedPreferences mSharedPreferences;
    private JSONArray mSeriesJSONArray;

    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        final InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-8810925182413877/5721373744");

        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
            }
        });

        mSharedPreferences = getSharedPreferences(SP_TAG, MODE_PRIVATE);

        mRecyclerView   = (RecyclerView)            findViewById(R.id.RecyclerView);
        mAddFAB         = (FloatingActionButton)    findViewById(R.id.AddFAB);

        installData();

        mRecyclerView.setAdapter(mAdapter = new Adapter(MainActivity.this, mSessionsList));
        mRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(MainActivity.this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new ItemDecoration());
        mRecyclerView.setHasFixedSize(true);

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);

        mRewardedVideoAd.loadAd("ca-app-pub-8810925182413877/3686237344", adRequest);

    }

    private void installData(){
        mSessionsList = new ArrayList<>();

        String spJSONArrayString = mSharedPreferences.getString(SDAROT_JSONARRAY_TAG, null);

        boolean has = spJSONArrayString != null;

        try {
            mSeriesJSONArray = has ? new JSONArray(spJSONArrayString) : new JSONArray();
        } catch (JSONException e) {
            has = false;
            mSeriesJSONArray = new JSONArray();
        }

        for(int i = 0; i < mSeriesJSONArray.length(); i++){
            JSONObject thisJSONObject;
            try {
                thisJSONObject = mSeriesJSONArray.getJSONObject(i);
                mSessionsList.add(new Series(
                        thisJSONObject.getString(Series.NAME_TAG),
                        thisJSONObject.getInt(Series.SEASON_TAG),
                        thisJSONObject.getInt(Series.EPISODE_TAG),
                        mOnAddOne));

            } catch (JSONException e) {
                continue;
            }
        }

        if(!has){
            updateData();
        }

        mSessionsList.add(new Series("רמזור", 1, 12));

    }

    public void updateData(){
        mSeriesJSONArray = new JSONArray();
        for (Series series : mSessionsList) {
            try {
                mSeriesJSONArray.put(
                        new JSONObject()
                                .put(Series.NAME_TAG, series.getName())
                                .put(Series.SEASON_TAG, series.getSeason())
                                .put(Series.EPISODE_TAG, series.getEpisode()));

            } catch (JSONException e) {
                continue;
            }
        }

        mSharedPreferences.edit().putString(SDAROT_JSONARRAY_TAG, mSeriesJSONArray.toString()).commit();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Toast.makeText(this, "Load", Toast.LENGTH_SHORT).show();

        mRewardedVideoAd.show();

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTitle;
        public TextView mSubTitle;
        public ImageButton mDeleteButton;

        public ViewHolder(View itemView, TextView Title, TextView SubTitle, ImageButton DeleteButton) {
            super(itemView);

            this.mTitle = Title;
            this.mSubTitle = SubTitle;
            this.mDeleteButton = DeleteButton;
        }
    }

    private Series.OnAddOne mOnAddOne = new Series.OnAddOne() {
        @Override
        public void onUserAddEpisode(int Position) {
            Series series = mSessionsList.get(Position);
            series.setEpisode(series.getEpisode() + 1);
            mAdapter.notifyItemChanged(Position);
            updateData();
        }
    };

    class Adapter extends RecyclerView.Adapter<ViewHolder> {

        private Context mContext;
        private Resources mResources;
        private ArrayList<Series> mSeesionsList;

        public Adapter(Context Context, @NonNull ArrayList<Series> SeesionsList){
            this.mContext = Context;
            this.mSeesionsList = SeesionsList;

            this.mResources = mContext.getResources();

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = ((LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_item, null);

            TextView    Title           = (TextView)        view.findViewById(R.id.TitleTextView);
            TextView    SubTitle        = (TextView)        view.findViewById(R.id.SubTitleTextView);
            ImageButton DeleteButton    = (ImageButton)     view.findViewById(R.id.DeleteButton);

            return new ViewHolder(view, Title, SubTitle, DeleteButton);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Series series = mSeesionsList.get(position);

            holder.mTitle.setText(series.getName());
            holder.mSubTitle.setText(
                    mResources.getText(R.string.season) + " " + Integer.toString(series.getSeason()) + " - " +
                    mResources.getText(R.string.episode) + " " + Integer.toString(series.getEpisode()));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(series.mOnAddOne != null) {
                        series.mOnAddOne.onUserAddEpisode(position);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mSeesionsList.size();
        }
    }

    class WrapContentLinearLayoutManager extends LinearLayoutManager {
        public WrapContentLinearLayoutManager(Context context) {
            super(context);
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, @NonNull RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("probe", "meet a IOOBE in RecyclerView");
            }
        }
    }

    class ItemDecoration extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.top = 15;
            outRect.bottom = 15;
            outRect.left = 10;
            outRect.right = 10;
        }
    }
}
